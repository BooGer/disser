# Репозиторий для работы с диссертацией

# Исследование оптимальных параметров и разработка полосковой антенны пассивной метки РЧИ для задач ретранслирования сигнала

## Цель:

Цель исследований определение оптимальных параметров всенаправленной полосковой антенны диапазонов диапазонов частот .... для задач последующей ретрансляции радиосигнала.

Разработка полосковой антенны пассивной метки РЧИ 

## Решаемые задачи:

1. На основании литературных источников изучить и обобщить опыт использования всенаправленных антенн для разработки устройств РЧИ; методов параметрический оптимизации микрополосковых антенн. Оформление соответствующего обзора.
2. На базе выбранных методов синтеза и оптимизации антенн с помощью программных комплексов спроектировать, промоделировать и исследовать полученные параметры полосковых антенн для поставленных задач. Выявить различия результатов, полученных различными методами.
3. На базе полученных результатов разработать масштабную модель микрополосковой антенны.
4. Испытания полосковой антенны в реальных условиях. Анализ и обработка результатов натурных испытаний
5. Оформления выпускной работы.

---

Изменить содержание обзора так чтобы оно соответствовало постановке задачи

Постановка задачи должна быть ограничена несколькими задачами и сводится к производству прототипа 1-2 антенн согласно расчетам

Сделать картинку или таблицу - классификация устройств систем РЧИ

Библиотеку из zotero переносить путем записи в файл экспортом.

Нумерация источников пока пофиг.

- [x]  Введение
- [ ]  Обзор по состоянию вопроса
    - [x]  Радиочастотная идентификация. Основные понятия и термины
        - [x]  Приемопередатчик
        - [x]  Сервер
        - [x]  Метка
            - [x]  Чип
            - [ ]  Антенна
    - [x]  Обзор литературных источников
        - [x]  Методы оптимизации и моделирования
        - [x]  Новые технологии предлагаемые для этих целей
        - [x]  Новые материалы и метки для сенсорных целей

            RSA - метод статистической поверхости

- [ ]  Теоретическая часть
    - [ ]  Теория микрополосковых антенн
    - [ ]  Методы синтеза микрополосковых антенн
    - [ ]  Методы параметрической оптимизации антенн
        - [ ]  метод оптимизации 1
        - [ ]  метод оптимизации 2
    - [ ]  Перспективные методы синтеза и оптимизации микрополосковых антенн
        - [ ]  метод 3
        - [ ]  метод 4
    - [ ]  Целевые параметры проектируемой антенны
- [ ]  Практическая часть
    - [ ]  Проектирование микрополосковой антенны заданных параметров
        - [ ]  метод 1
        - [ ]  метод 2
        - [ ]  метод 3
    - [ ]  Моделирование топологии антенны
        - [ ]  модель антенна 1
        - [ ]  модель антенна 2
        - [ ]  модель антенна 3
- [ ]  Экспериментальная часть
    - [ ]  Разработка масштабной модели антенны
        - [ ]  выбранная антенна
    - [ ]  Испытательный стенд
        - [ ]  описание стенда
        - [ ]  описание хода эксперимента
        - [ ]  Полученные результаты
- [ ]  Заключение

